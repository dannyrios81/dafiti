<?php

use PHPUnit\Framework\TestCase;
use \App\Poker;

class PokerTest extends TestCase
{
    public function testAlgorithm()
    {
        $poker = new Poker();
        $results1 = $poker->isStraight([2,3,4,5,6]);
        $this->assertEquals($results1,true,"2,3,4,5,6");

        $results2 = $poker->isStraight([14,5,4,2,3]);
        $this->assertEquals($results2,true,"14,5,4,2,3");

        $results3 = $poker->isStraight([7,7,12,11,3,4,14]);
        $this->assertEquals($results3,false,"7,7,12,11,3,4,14");

        $results4 = $poker->isStraight([7,3,2]);
        $this->assertEquals($results4,false,"7,3,2");

//        ejemplos

        $results5 = $poker->isStraight([14,2,3,4,5]);
        $this->assertEquals($results5,true,"14,2,3,4,5");

        $results6 = $poker->isStraight([9,10,11,12,13]);
        $this->assertEquals($results6,true,"9,10,11,12,13");

        $results7 = $poker->isStraight([2,7,8,5,10,9,11]);
        $this->assertEquals($results7,true,"2,7,8,5,10,9,11");

        $results8 = $poker->isStraight([7,8,12,13,14]);
        $this->assertEquals($results8,false,"7,8,12,13,14");
    }
}
