# Preguntas

+ Supone que en un repositorio GIT hiciste un commit y te olvidaste un archivo.<br>
   Explicar como se soluciona si hiciste push, y como si aun no hiciste. De ser posible, buscar que quede solo un commit con los cambios.
   >### Respuesta
   >```
   >git add FILE
   >git commit --amend 
   >git pull origin BRANCH-NAME
   >git push -f origin BRANCH-NAME
   >```

+ Tenes un sitio en tu computadora de desarrollo, y cuando entras desde el navegador, en la consola te aparece esto:
  ```html
  https://site.local/favicon.ico	Failed to load resource: net::ERR_INSECURE_RESPONSE
  ```
  El archivo existe, el sitio debe cargar por https. Como lo solucionas?
  
  >### Respuesta<br>
  >El error en mi experiencia se me ha presentado cuando estoy usando un certificado de seguridad autofirmado, por lo 
  cual la forma mas sencilla de resolucion es usar un servicio como Let's Encrypt para generar el certificado digital 
  para los ambientes locales de desarrollo 

+ Tenes un archivo comiteado en un repositorio GIT que deseas que quede ignorado. Que pasos debes realizar?
   >### Respuesta
   >```
   >git rm FILE
   >```
   >_crear archivo .gitignore si no existe_<br>
   >_agregar el nombre del archivo a ignorar al archivo .gitignore_
   >```
   >git add .gitignore
   >git commit -m "MENSAJE"
   >```
   
+ Explica las ventajas de cargar en tu sitio las librerias de terceros por GTM.
  >### Respuesta<br>
  >las ventajas principales son, mantener un codigo limpio ya que las etiquetas se encuentran dentro del contenedor y no
   regadas por toda la pagina y la carga de los codigos de terceros son cargados de manera asincronica, lo cual evita 
   que la pagina presente lentitud en su carga


## Ejercicio	(puede	ser	php	o no)

+ Escribir	una	funcion	que	determine	si	un	conjunto	de	cartas	de	una	lista	representan	una	escalera	de	poker	(5
  cartas	con	valores	consecutivos)	o	no. <br>
  Las	cartas	siempre	tienen	valores	entre	2	y	14,	donde	14	es	el	AS.<br>
  Tener	en	cuenta	que	el	AS	tambien	cuenta	como	1.<br>
  La	cantidad	de	cartas	puede	variar,	pero	nunca	es	superior	a	7.<br>
  
  Ejemplo
  ```
  escalera:	14-2-3-4-5
  escalera:	9-10-11-12-13
  escalera:	2-7-8-5-10-9-11
  no es escalera:	7-8-12-13-14
  ```
  La	funcion	debe	validar	un	caso	de	pruebas	similar	a	este:
  
  ```php
  class Poker extends TestCase
  {
    public function testAlgorithm() {
        $results1=isStraight([2,3,4,5,6]);
  		$this->assertEquals($results1,true,"2,3,4,5,6");
  		$results2=isStraight([14,5,4,2,3]);
  		$this->assertEquals($results2,true,"14,5,4,2,3");
  		$results3=isStraight([7,7,12,11,3,4,14]);
  		$this->assertEquals($results3,false,"7,7,12,11,3,4,14");
  		$results4=isStraight([7,3,2]);
  		$this->assertEquals($results4,false,"7,	3,2");
  	}
  }
  ```
  
  >### Respuesta
  >La respuesta a este punto se encuentra en la carpeta app de este repositorio, asi como tambien en la carpeta test se 
  >encuentra la prueba automatizada
  
## MySQL
+ Dado	el	siguiente	esquema	de	tablas:<br>
![ER!](images/er.png)

Escribir	las	queries	para:
+ ¿Cuál	es	el	jugador	más	viejo	de	cada	equipo?
>### Respuesta
>```
> SELECT 
>jugadores.id_jugadores,
>jugadores.fk_equipos,
>jugadores.nombre,
>jugadores.fecha_nacimiento
>FROM jugadores
>GROUP BY fk_equipos
>ORDER BY `fecha_nacimiento` ASC 
>```
+ ¿Cuántos	partidos	jugó	de	visitante	cada	equipo?	(nota:	hay	equipos	no	jugaron	ningún	partido)
>### Respuesta
>```
>SELECT
>e.nombre,
>count(*) partidos_jugados
>FROM
>partidos p
>INNER JOIN equipos e on e.id_equipos=p.fk_equipo_visitante
>GROUP BY
>p.fk_equipo_visitante
>```
+ ¿Qué	equipos	jugaron	el	01/01/2016	y	el	12/02/2016?
>### Respuesta
>```
>SELECT
>l.nombre equipo_local,
>v.nombre equipo_visitante,
>p.fecha_partido
>FROM
>partidos AS p
>INNER JOIN equipos AS l ON p.fk_equipo_local = l.id_equipos
>INNER JOIN equipos AS v ON p.fk_equipo_visitante = v.id_equipos
>WHERE
>p.fecha_partido BETWEEN '2016-01-01' AND '2016-02-12'
>```
+ Diga	el	total	de	goles	que	hizo	el	equipo	“Chacarita”	en	su	historia	(como	local	o	visitante)
>### Respuesta
>```
>SELECT 
>(SELECT SUM(p1.goles_local) FROM partidos p1 WHERE p1.fk_equipo_local = e.id_equipos) + 
>(SELECT SUM(p2.goles_visitante) FROM partidos p2 WHERE p2.fk_equipo_visitante = e.id_equipos) total_goles
>FROM 
>equipos e
>WHERE
>e.nombre = "Chacarita";
>```
##Extra
+ Contanos	en	pocas	lineas	cual	crees	que	es	la	mayor	innovacion	en	el	mundo	del	desarrollo	en	los	ultimos	5
años,	y	por	que.

>#### Respuesta<br>
>La innovacion que mas me tiene asombrado por todo lo que ha logrado teniendo en cuenta que no hace mucho empezo a cojer fuerza
>de manera exponencial, es la AI, ya que hasta hace un par de años hablar de ello no era mas que ciencia ficcion o solo 
>para trabajo cientifico, ahora se tiene al alcance de la mano, con un par de librerias y unas cuantas horas de codigo 
>se puede crear un chatbot con reconocimiento de lenguaje natural y probablemente este sea capaz de pasar una prueba de turing
