<?php


namespace App;



class Poker
{

    public function isStraight(Array $array)
    {

        if (count($array)<5)
            return false;

        sort($array);


        $this->completeArray($array);


        for ($i=0;$i<count($array)-4;$i++)
        {


            if($this->isConsecutive(array_slice($array,$i,5)))
                return true;
        }

        return false;
    }
    private function completeArray(&$array)
    {
        $array = array_merge($array,array_slice($array,0,4));
    }
    function isConsecutive($array) {
        if(in_array(14,$array) and in_array(2,$array))
            $array[array_search(14,$array)]=1;

        return ((int)max($array)-(int)min($array) == (count($array)-1));
    }
}